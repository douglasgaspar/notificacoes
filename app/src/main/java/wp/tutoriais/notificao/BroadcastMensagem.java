package wp.tutoriais.notificao;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class BroadcastMensagem extends BroadcastReceiver {

    //Assim que o botão de ação da notificação for acionado, o método
    //onReceive() será executado
    @Override
    public void onReceive(Context context, Intent intent) {
        //Ao ser executado, deverá criar um Toast para validação do seu funcionamento
        Toast.makeText(context, "Teste do botão de ação", Toast.LENGTH_SHORT).show();

    }
}