package wp.tutoriais.notificao;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

public class NovaTela extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nova_tela);

        //Criar um objeto para o botão da tela
        Button btVoltar = findViewById(R.id.btVoltar);

        //Criar um evento para ser acionado ao clicar sobre o botão
        btVoltar.setOnClickListener(v ->{
            //Criando uma Intent que irá direcionar para a tela principal (MainActivity)
            Intent itVoltar = new Intent(this, MainActivity.class);
            //Requisitar a abertura da tela
            startActivity(itVoltar);
        });
    }
}