package wp.tutoriais.notificao;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    private final String ID_CANAL_NOTIFICACAO = "canal_notificacoes_wp";
    private String textoGrande = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam tempor laoreet justo eget lacinia. Vestibulum rhoncus facilisis magna eget porttitor. Suspendisse potenti. Nulla semper, purus nec scelerisque efficitur, nisi erat pulvinar magna, maximus dapibus risus lectus sit amet diam. Duis tincidunt tempor nisl ac interdum. Aliquam erat volutpat. Cras nec egestas odio.";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Antes de criar qualquer notificação, devemos criar o canal de notificações
        //Lembrando que o canal só será criado se o dispositivo estiver executando a versão 8
        //do Android ou versão posterior a essa
        criaCanalNotificacao();

        //Criando objetos para cada componente da tela
        Button btSimples = findViewById(R.id.btSimples);
        Button btTextoNotificacao = findViewById(R.id.btTexto);
        Button btImagemNotificacao = findViewById(R.id.btImagem);
        Button btBotaoAcao = findViewById(R.id.btAcao);

        //Evento que será executado ao clicar sobre o botão de notificação simples
        btSimples.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificacaoBasica();
            }
        });

        btTextoNotificacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificacaoTexto();
            }
        });

        btImagemNotificacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificacaoImagem();
            }
        });

        btBotaoAcao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                notificacaoBotaoAcao();
            }
        });
    }

    private void notificacaoBotaoAcao(){
        //Como a ação será executada posteriormente, ou seja, depois que a notificação for exibida
        //devemos criar uma Intent para indicar o que será executado e transforma-la em PendingIntent
        Intent exibirMensagem = new Intent(this, BroadcastMensagem.class);
        PendingIntent pi = PendingIntent.getBroadcast(this, 0, exibirMensagem, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder configuraNotificacao = new NotificationCompat.Builder(this, ID_CANAL_NOTIFICACAO)
                .setSmallIcon(R.drawable.icone_notificacao) //Ícone da barra de status - obrigatório
                .setContentTitle("Notificação com botão") //Título da notificação
                .setContentText("Clique no botão abaixo para exibir a mensagem") //Texto da notificação
                .setAutoCancel(true) //Remove a notificação após o toque sobre ela
                .addAction(R.drawable.message_toast, getString(R.string.acaoNotificacao), pi)
//                .addAction(R.drawable.message_toast, getString(R.string.acaoSim), pi)
//                .addAction(R.drawable.message_toast, getString(R.string.acaoNao), pi)
//                .addAction(R.drawable.message_toast, getString(R.string.acaoTalvez), pi)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT);

        //Gerenciador de notificações do sistema
        NotificationManagerCompat gerenciadorNotificacao = NotificationManagerCompat.from(this);
        gerenciadorNotificacao.notify(400, configuraNotificacao.build());
    }

    private void notificacaoImagem(){
        //Criar um objeto Bitmap para adicionar na configuração da notificação
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.android);

        NotificationCompat.Builder configuraNotificacao = new NotificationCompat.Builder(this, ID_CANAL_NOTIFICACAO)
                .setSmallIcon(R.drawable.icone_notificacao) //Ícone da barra de status - obrigatório
                .setContentTitle("Notificação com imagem") //Título da notificação
                .setContentText("Essa notificação possui imagem maior. Expanda a caixa para visualizar o restante.") //Mensagem interna da notificação
                .setLargeIcon(bitmap) //Exibe um ícone menor da imagem ao lado do texto da notificação
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap) //Adicionando a imagem maior
                        .bigLargeIcon(null)) //Não mostra o ícone quando a imagem maior é exibida
                .setAutoCancel(true) //Remove a notificação após o toque sobre ela
                .setPriority(NotificationCompat.PRIORITY_DEFAULT); //Prioridade padrão

        //Gerenciador de notificações do sistema
        NotificationManagerCompat gerenciadorNotificacao = NotificationManagerCompat.from(this);
        gerenciadorNotificacao.notify(300, configuraNotificacao.build());
    }

    private void notificacaoTexto(){
        NotificationCompat.Builder configuraNotificacao = new NotificationCompat.Builder(this, ID_CANAL_NOTIFICACAO)
                .setSmallIcon(R.drawable.icone_notificacao) //Ícone da barra de status - obrigatório
                .setContentTitle("Notificação com texto extenso") //Título da notificação
                .setContentText("Essa notificação possui mais texto. Expanda a caixa para visualizar a mensagem abaixo.") //Mensagem interna da notificação
                .setStyle(new NotificationCompat.BigTextStyle().bigText(textoGrande))
                .setAutoCancel(true) //Remove a notificação após o toque sobre ela
                .setPriority(NotificationCompat.PRIORITY_DEFAULT); //Prioridade padrão

        //Gerenciador de notificações do sistema
        NotificationManagerCompat gerenciadorNotificacao = NotificationManagerCompat.from(this);
        gerenciadorNotificacao.notify(200, configuraNotificacao.build());
    }

    private void notificacaoBasica(){
        //Caso queiramos abrir uma Activity ou parte específica do nosso aplicativo quando
        //o usuário tocar sobre a notificação, devemos criar uma Intent e transformá-la em PendingIntent
        Intent abrirTela = new Intent(this, NovaTela.class);
        //Caso queira passar valores de controle para serem recebidos na "NovaTela" é possível usar o método putExtra()
        //Exemplo: abrirTela.putExtra("valorEnvio", 950.10);
        //Transformar a Intent em PendingIntent pois ela ficará "pendente", ou seja, será
        //executada posteriormente, isto é, só quando for acionada e não logo que a notificação aparecer
        PendingIntent pi = PendingIntent.getActivity(this, 0, abrirTela, PendingIntent.FLAG_CANCEL_CURRENT);


        //Utilizando a classe do NotificationCompat que possibilita a criação de notificações
        //tanto para as versões mais novas do Android quanto mais antigas.
        //A classe Builder é utilizada para configuração da notificação
        NotificationCompat.Builder configuraNotificacao = new NotificationCompat.Builder(this, ID_CANAL_NOTIFICACAO)
                .setSmallIcon(R.drawable.icone_notificacao) //Ícone da barra de status - obrigatório
                .setContentTitle("Título da notificação") //Título da notificação
                .setContentText("Parabéns! Nossa primeira notificação no Android.") //Mensagem interna da notificação
                .setAutoCancel(true) //Remove a notificação após o toque sobre ela
                .setContentIntent(pi) //Qual recurso será aberto ao tocar na notificação
                .setPriority(NotificationCompat.PRIORITY_DEFAULT); //Prioridade padrão

        //O código acima, utilizado a classe Builder, serve apenas para configuração
        //Para que a notificação seja enviada ao sistema e exibida é necessário pegar
        //a configuração acima e enviar ao gerenciador de notificações do sistema

        //Criando um objeto que irá representar o gerenciador de notificações do sistema
        NotificationManagerCompat gerenciadorNotificacao = NotificationManagerCompat.from(this);

        //Chamando o método notify() para criar a notificação com as configurações acima e exibi-la
        //O ID, no caso colocado como 100, serve para representar essa notificação em específico.
        //Com ele podemos acessar essa notificação posteriormente e removê-la ou modificá-la
        //O ID pode ser qualquer valor que desejar.
        gerenciadorNotificacao.notify(100, configuraNotificacao.build());
    }

    //Como o canal de notificação é obrigatório no Android 8 e versões após ele
    //é necessário verificar qual a versão do dispositivo e criar o canal
    //somente se for igual ao Android 8 ou maior que ele
    private void criaCanalNotificacao() {
        //Se a versão do SDK do dispositivo for maior ou igual a versão 8 (Oreo)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            //Atribui um nome para o canal
            CharSequence nomeCanal = getString(R.string.nomeCanal);
            //Atribui-se uma descrição para qual a finalidade desse canal
            String descricaoCanal = getString(R.string.descricaoCanal);

            //Cria-se o canal de notificação
            NotificationChannel canal = new NotificationChannel(ID_CANAL_NOTIFICACAO, nomeCanal, NotificationManager.IMPORTANCE_DEFAULT);
            //Adiciona a descrição
            canal.setDescription(descricaoCanal);

            //Por fim, devemos enviar o nosso canal criado no código acima para o gerenciador
            //de notificações do Android para que o nosso canal faça parte do sistema e esteja
            //disponível para uso, ou seja, assim podemos enviar notificações nele
            NotificationManager gerenciadorNotificacao = getSystemService(NotificationManager.class);
            gerenciadorNotificacao.createNotificationChannel(canal);
        }
    }
}